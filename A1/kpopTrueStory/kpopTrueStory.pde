import processing.sound.*;
import processing.video.*;

// Text
String[] subtitle = {"Their lives may look fascinating from an outsider’s \n perspective, but many struggle with severe stress",
"Under intense pressure to maintain their popularity, and worries\n about a potential breakup of the band or celebrity scandals and gossip",
"trainees will not hesitate to fix their\n appearance for a better career prospect",
"the heavy workload has resulted in quite a lot\n of car accidents and crashes, which can sometimes turn deadly",
"you work and work and work.",
"you get hospitalized for exhaustation, but your managers drag\n you out of the A&E and throw you right back into your schedule",
"don’t forget about your contract with the agency",
"the more successful you are, the more hate you get",
"you must always maintain an idol image",
"you are constantly watched by fans and haters",
"you are losing out on a “normal” childhood.",
"You live in a cramped room with up to ten\n other people and have no privacy",
"you can skip attending school during promotions\n but still need to do HW/online classes to graduate",
"fans tell you you’re fat, so you go on a diet.\n Then people start saying you have no boobs/muscles",
"you fall in love and want to date but your contract says no dating",
"if needed, the agencies may pressurize you to get plastic surgeries",
"you may have to stay at a place far away from your family and friends",
"fans stalk you and go to your house, scare your family, attack\n you and tail your car, send scary love letters to you…",
"being a K-pop idol is almost torture",
"how long can you be an idol?",
"you miss your family so much but you don’t have time to see them",
"you could constantly be on a diet till you’re the desired weight",
"you know you’re making a lot, but your company takes most of it away",
"‘Oh, she must be like that in real life as well",
"agency pressured female kpop stars into sexy concept",
"you see the same people all day, every day",
"if you’re unsuccessful, you would be jobless and\n there’s really nothing for you to do to earn a living",
"give up on everything and focus on training to be one",
"we had to share two or three mattresses",
"we had nothing to eat",
"you will have a hectic schedule",
"no matter what struggles you’re going through,\n you still have to keep your smile on for the stage",
"One day we will…",
"if you’re late or go against the rules you have to sing\n while running around the practice room 10 times.",
"you write about what you did wrong and you aren’t allowed to practice",
"I wanted to go to school and eat and hang out with friends so I cried a lot",
"1 or 2 people will get cut, so that the rest would work harder",
"I don't think there is anything as tough as being a trainee in Korea",
"I think I consumed about 300 calories a day",
"I really had no strength",
"you get hate comments insulting your family, your friends, the smallest things you do",
"I thought I was going to die",
"you get comments talking about you like you’re just a thing,\n talking about how fans want to touch your face or hair or other…parts...",
"I couldn't even drink water. for real",
"you want to sleep and feel like collapsing 24/7, but your company tells you to keep training, singing, acting, performing…",
};

int subtitleNum = subtitle.length;
PFont f;

// Audio and Video
SoundFile backgroundMusic;
SoundFile[] audio;
Movie[] clip;

// Number of samples
int audioNum = 45;
int clipNum = 29;
int clipNow;
float movieDur = 0;

// playSound array defines what will be triggered
int[] playSound = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// clipDur defines how long each clip plays
int[] clipDur = {7, 8, 9, 10, 7, 9, 8, 10, 11, 8, 9, 7, 10, 7, 10, 8, 10, 9, 10, 7, 8, 10, 10, 9, 10, 9, 10, 8, 10};

// Trigger keeps track of time. 
int trigger;
int movieTrigger;

// Text Agent to maintain text on screen
ArrayList<Agent> agents;
int agentsCount;

// GUI set up
Gui gui;
float subtitle_freq = 1.5;
int max_voice_gap = 500;


void createAgents() {
  background(255);
  agents = new ArrayList<Agent>();
  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent(random(0,1000), random(0,720), subtitle[int(random(0,45))]);
    a.setTime();
    agents.add(a);
  }
}

void movieEvent(Movie m) {
  m.read();
}

void setup(){
  size(1280, 720);
  colorMode(HSB, 360, 100, 100);
  background(255);
  
  // Create an array of empty soundfiles
  audio = new SoundFile[audioNum];
  clip = new Movie[clipNum];
  
  // set up custome font and size
  f = createFont("sad.ttf", 16, true); // Arial, 16 point, anti-aliasing on
  smooth();
  textFont(f,54);
  
  // set up text agent
  agentsCount = subtitleNum;
  createAgents();
  
  // Load all soundfiles from a folder in a for loop and lower the volume to balance with the background music
  for (int i = 0; i < audioNum; i++){
    audio[i] = new SoundFile(this, (i+1) + ".mp3");
    audio[i].amp(0.2);
  }
  
  // load clips similarly
  for (int i = 0; i < clipNum; i++){
      clip[i] = new Movie(this, (i+1) + ".m4v");
  }
  
  // load background music and loop it
  backgroundMusic = new SoundFile(this, "background.mp3");
  backgroundMusic.loop();
  
  // Create a trigger which will be the basis for our random sequencer. 
  trigger = millis();
  
  // setup the simple Gui
  gui = new Gui(this);
  gui.addSlider("subtitle_freq", 0.0, 5.0);
  gui.addSlider("max_voice_gap", 300, 1000);
}


void draw(){
  background(0);
  image(clip[clipNow], 0, 0, 1280, 720);
  
  // update and draw all the agents
  for (Agent a : agents) {
    a.update();
  }
  
  for (Agent a : agents) {
    a.draw();
  }
  
  // trigger the movie randomly with it's duration, also set volume to 0
  if ((millis() - movieTrigger) / 1000.0 > movieDur){
    clipNow = int(random(0,29));
    clip[clipNow].stop();
    clip[clipNow].play();
    clip[clipNow].volume(0);
    movieDur = clipDur[clipNow];
    movieTrigger = millis();
  }
  
  // If the determined trigger moment in time matches up with the computer clock events get triggered.
  if (millis() > trigger){
    for (int i = 0; i < audioNum; i++){      
      // Check which indexes are 1 and 0.
      if (playSound[i] == 1){
          audio[i].stop();
          audio[i].play();
      }
      
      // Renew the indexes of playSound so that at the next event the order is different and randomized.
      playSound[i] = random(1000) < subtitle_freq ? 1 : 0;
    } //<>//
    
    // creates an offset between .2 and max_voice_gap second
    trigger = millis() + int(random(200, max_voice_gap));
  }
  
}
