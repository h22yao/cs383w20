import processing.video.*;
import processing.sound.*;
import gab.opencv.*;
import org.opencv.imgproc.Imgproc;
import java.awt.Rectangle;
Capture cam;
OpenCV opencv;
float scale = 0.5;
float darkScale = 1;
ArrayList<Contour> contours;
ArrayList<Contour> polygons;
PImage debug;
PImage[] effects;
SoundFile[] powers;
SoundFile no;
SoundFile shot;
SoundFile backgroundMusic;

PImage output;
PVector directionRight;
PVector directionLeft;
PVector directionRightNorm;
PVector directionLeftNorm;
// the interactive drawing visualization
PGraphics viz;
boolean darkMode = false;
boolean countDark = true;
boolean countGrey = true;

// value need to be set to show animation
int startTime = 0;
float leastDarkTime = 4.5;
PImage img;
PImage knife;
PImage hole;
float knifeTimer = 0;
float holeTimer = 0;
int effNum = 0;
boolean firstTime = true;

PVector getFlowInRegion(int x, int y, int w, int h) {
      // calculate average movement vector in region
    PVector v = opencv.getAverageFlowInRegion(x, y, w, h);
    // if motion is very small, optical flow might return NaN
    // if this happens, just create a 0 vector
    if (Float.isNaN(v.x) || Float.isNaN(v.y)) {
      v = new PVector();
    }
    return v;
}

void startBackground() {
  // history – Length of the history.
  // nmixtures – Number of Gaussian mixtures.
  // backgroundRatio – how fast forground objects blend into background
  opencv.startBackgroundSubtraction(5, 3, 0.0001);
}

float adjustX(float low, float high) {
  float v = map(mouseX, 0, width - 1, low, high); 
  println("adjustX: ", v);
  return v;
}

void setup() {
  fullScreen();
  background(0);
  // want video frame and opencv proccesing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));
  opencv = new OpenCV(this, cam.width, cam.height);
  cam.start();
  no = new SoundFile(this, "no.mp3");
  shot = new SoundFile(this, "shooting.mp3");
  backgroundMusic = new SoundFile(this, "paintingGrey.mp3");
  backgroundMusic.play();
  startBackground();
  output = new PImage(cam.width, cam.height);
  //lastPoint = new PVector(width/2, height/2);
  directionRight = new PVector(0, 0);
  directionLeft = new PVector(0, 0);
  viz = createGraphics(width, height);
  effects = new PImage[3];
  img = loadImage("hole.png");
  knife = loadImage("knifeTopRight.png");
  hole = loadImage("hole.png");
  powers = new SoundFile[3];
  for (int i = 0; i < 3; i++) {
      effects[i] = loadImage((i+1) + ".png");
      powers[i] = new SoundFile(this, (i+1) + ".mp3");
      powers[i].amp(0.2);
  }
}


void draw() {
  if (cam.available() == true) {
    cam.read();
    opencv.loadImage(cam);
    opencv.flip(1);
    
    if(!darkMode) {
      countDark = true;
      opencv.calculateOpticalFlow();
      // right side
      directionRight = getFlowInRegion(cam.width/2, 0, cam.width/2, cam.height);
      // left side
      directionLeft = getFlowInRegion(0, 0, cam.width/2, cam.height);
      directionRightNorm = directionRight.normalize();
      directionLeftNorm = directionLeft.normalize();
      if(countGrey){
        startTime = millis();
        countGrey = false;
      }
      output = opencv.getSnapshot(); 
      
      float imageScale = height / (float)output.height;
      
      pushMatrix();
      translate(width/2 - (output.width*imageScale)/2, 0);
      scale(imageScale);
      image(output, 0, 0);
      popMatrix();
      
      // draw direction vector for debugging purposes
      //stroke(255, 255, 0, 128);
      //strokeWeight(4);
      //PVector a = new PVector(width * .75, height/2);
      //PVector b = PVector.add(a, PVector.mult(directionRight, 50));
      //line(a.x, a.y, b.x, b.y);
      //// draw direction vectors
      //stroke(255, 0, 255, 128);
      //strokeWeight(4);
      //a = new PVector(width * .25, height/2);
      //b = PVector.add(a, PVector.mult(directionLeft, 50));
      //line(a.x, a.y, b.x, b.y);
      //print(directionRight);
      //print(directionLeft + "\n");
      
      if((millis() - startTime) / 1000 > leastDarkTime) {
        if (directionRightNorm.x > 0.40 && directionRightNorm.y > 0.45 && directionLeftNorm.x < -0.40 && directionLeftNorm.y > 0.45) {
          holeTimer = millis();
          shot.stop();
          shot.amp(0.2);
          shot.play();
        } else if((directionRightNorm.y > 0.95 && directionLeftNorm.y > 0.95) || directionRight.y > 0 && directionLeft.y > 0 && directionRight.y + directionLeft.y > 2.2) { // dark mode
          countGrey = true;
          darkMode = true;
        } else if (directionRightNorm.x > 0.5 && directionRightNorm.y < -0.5 && directionLeftNorm.x > 0.5 && directionLeftNorm.y < -0.5) { //knife out
          knifeTimer = millis();
        } else if (directionRight.y < -0.7 && directionLeft.y > 0.7) {
          no.stop();
          no.amp(0.5);
          no.play();
        }
      }
      
      if(millis() - knifeTimer < 1000) {
        image(knife, cam.width / 2 * imageScale - 80, cam.height / 2 * imageScale - 80, 200, 200);
      } else if (millis() - holeTimer < 2000) {
        for (int i = 0; i < 30; i++) {
          float size = random(100, 180);
          float xPos = random(-500, 700);
          float yPos = random(-500, 500);
          image(hole, cam.width / 2 * imageScale + xPos, cam.height / 2 * imageScale + yPos, size, size); 
        }
      }
      
    } else { // darkMode goes here
      if (countDark) {
        startTime = millis();
        countDark = false;
      }
      if (keyPressed) {
        startBackground();
      }
      opencv.updateBackground();
      opencv.close(2);
      debug = opencv.getSnapshot(); // grab debug image here
      float imageScale = height / (float)debug.height;
      // find contours sorted by descending area
      // set param 1 to true to find nested contours (holes in blobs)
      // set param 2 to true to return contours sorted by descending area
      contours = opencv.findContours(false, true);
      
      // draw the cam full screen
      pushMatrix();
      translate(width/2 - (debug.width*imageScale)/2, 0);
      scale(imageScale);
      image(debug, 0, 0);
      popMatrix();
      
      if (contours != null) {
        for (int i = 0; i < contours.size(); i++) {
          Contour c = contours.get(i);
          if(firstTime) {
            effNum = (int)random(0, 3);
            firstTime = false;
            powers[effNum].play();
            print("first time" + effNum + "\n");
          }
          float minArea = 1500; //adjustX(100, 10000);
          float maxArea = 17500; //adjustX(15000, 45000);
          if (c.area() > minArea) {
            Rectangle boundingBox = c.getBoundingBox();
            float squareLeftX = boundingBox.x * imageScale + width/2 - (debug.width*imageScale)/2;
            float squareWidth = boundingBox.width * imageScale;
            
            // drawing debug red rect
            //stroke(255, 0, 0);
            //strokeWeight(5);
            //noFill();
            //rect(squareLeftX, boundingBox.y * imageScale, squareWidth, boundingBox.height * imageScale);
            
            if(squareLeftX < cam.width / 2 * imageScale - 290) { //if the red square is on the left excluding middle
              image(effects[effNum], squareLeftX, boundingBox.y * imageScale - 250, 200, 200);
              if(!powers[effNum].isPlaying()) powers[effNum].play();
            } else if (squareLeftX > cam.width / 2 * imageScale + 260) { //if the red square is on the right excluding middle
              image(effects[effNum], squareLeftX + squareWidth - 200, boundingBox.y * imageScale - 250, 200, 200);
              if(!powers[effNum].isPlaying()) powers[effNum].play();
            }
          }
          // do not go back to the other mode immediately
          if((millis() - startTime) / 1000 > leastDarkTime) {
            if (c.area() > maxArea) {
              countDark = true;
              darkMode = false;
              firstTime = true;
            }
          }
        }
      }
    }
  }
}
