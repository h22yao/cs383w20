class Agent {
  // current position
  float x, y;
  // previous position
  float px, py;
  float xOff, yOff;
  String subtitle;
  int time;
  // values set up for ease in and out the text
  float value = 0.0;
  int MAX = 255;
  float text_speed = 5.0;
  
  float text_duration = 3;
  float text_chance = 1;
  
  // create agent that picks starting position itself
  Agent() {
    // default is all agent are at centre
    x = width / 2;
    y = height / 2;
  }

  // create agent at specific starting position
  Agent(float _x, float _y, String s) {
    x = _x;
    y = _y;
    xOff = random(-1, 1);
    yOff = random(-1, 1);
    subtitle = s;
  }

  void setTime() {
   time = millis(); 
  }
  
  void update() {
    // save last position
    px = x;
    py = y;
    
    // pick a new position
    x = x + xOff;
    y = y + yOff;
    
  }

  void draw() {
    if (random(1000) < text_chance) {
      setTime();
      x = random(0,1000);
      y = random(0,720);
      
    }
    if(millis() < time + text_duration * 1000){
      textAlign(LEFT);
      value += text_speed;
      float fade = ((sin(radians(value))+1)/2) * MAX;
      fill(255, fade);
      text(subtitle, x, y);
    }
  }
}
